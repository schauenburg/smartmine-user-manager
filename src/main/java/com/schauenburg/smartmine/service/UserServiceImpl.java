package com.schauenburg.smartmine.service;

import com.schauenburg.smartmine.model.security.Authority;
import com.schauenburg.smartmine.model.security.User;
import com.schauenburg.smartmine.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('USER_READ') and hasAuthority('USER_UPDATE')")
    public User get(Long id) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('USER_READ')")
    public List<User> getAllUsers() {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('USER_READ')")
    public User getUserByUsername(String username) {
        return this.userRepository.findByUsername(username);
    }

    @Override
    @Transactional
    @PreAuthorize("hasAuthority('USER_CREATE')")
    public User createUser(User user) {
        return null;
    }

    @Override
    @Transactional
    @PreAuthorize("hasAuthority('USER_UPDATE')")
    public User updateUser(User user) {
        return null;
    }

    @Override
    @Transactional
    @PreAuthorize("hasAuthority('USER_DELETE')")
    public void deleteUser(Long id) {

    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('USER_READ')")
    public Authority get(String name) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('AUTHORITY_READ')")
    public List<Authority> getAllAuthorities() {
        return null;
    }
}
