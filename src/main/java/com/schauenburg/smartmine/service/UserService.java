package com.schauenburg.smartmine.service;

import com.schauenburg.smartmine.model.security.Authority;
import com.schauenburg.smartmine.model.security.User;

import java.util.List;

public interface UserService {
    User get(Long id);
    List<User> getAllUsers();
    User getUserByUsername(String username);
    User createUser(User user);
    User updateUser(User user);
    void deleteUser(Long id);

    Authority get(String name);
    List<Authority> getAllAuthorities();
}
